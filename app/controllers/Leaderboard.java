package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import utils.MessageDateComparator;
import utils.UserSocialComparator;
import utils.UserTalkativeComparator;
import utils.UserLeastTalkativeComparator;

public class Leaderboard extends Controller
{

	public static void index()
	{
		if (session.get("logged_in_userid") != null)
		{
			List <User> users = User.findAll();
			Collections.sort(users, new UserSocialComparator());
			render("leaderboard/index.html", users);
		}
		else
		{
			Accounts.login();
		}

	}

	public static void talkative()
	{
		List <User> users = User.findAll();
		Collections.sort(users, new UserTalkativeComparator());
		render("leaderboard/index.html", users);
	}

}