package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;
import play.db.jpa.Blob;

public class EditProfile extends Controller
{
	public static void index()
	{
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		render(user);
	}

	public static void editDetails(String firstName, String lastName, String email, String password, String nationality, int age)
	{
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		user.firstName = firstName;
		user.lastName = lastName;
		user.email = email;
		user.password = password;
		user.nationality = nationality;
		user.age = age;
		user.save();
		Logger.info("Details changed to: " + firstName + lastName + email + password + nationality + age);
		Profile.index();
	}

	public static void editFirstName(String firstName)
	{
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		user.firstName = firstName;
		Logger.info("First Name changed to: " + firstName);
		Profile.index();
	}

	public static void editLastName(String lastName)
	{
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		user.firstName = lastName;
		Logger.info("Last Name changed to: " + lastName);
		Profile.index();
	}

	public static void editEmail(String email)
	{
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		user.email = email;
		Logger.info("Email changed to: " + email);
		Profile.index();
	}

	public static void editPassword(String password)
	{
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		user.password = password;
		Logger.info("Password changed to: " + password);
		Profile.index();
	}

	public static void editNationality(String nationality)
	{
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		user.nationality = nationality;
		Logger.info("Nationality changed to: " + nationality);
		Profile.index();
	}

	public static void editAge(int age)
	{
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		user.age = age;
		Logger.info("Age changed to: " + age);
		Profile.index();
	}
}