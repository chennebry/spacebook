package controllers;

import play.*;
import play.mvc.*;
import utils.MessageDateComparator;
import utils.MessageFromComparator;

import java.util.*;

import models.*;

public class Home extends Controller
{
	public static void index()
	{
		if (session.get("logged_in_userid") != null)
		{
			String userId = session.get("logged_in_userid");
			User user = User.findById(Long.parseLong(userId));

			ArrayList<ArrayList<Message>> conversations = new ArrayList<>();
			ArrayList<Message> sortedList = new ArrayList<Message>();
			sortedList.addAll(user.inbox);
			Collections.sort(sortedList, new MessageDateComparator());
			conversations.add(sortedList);
			render("home/index.html", user, conversations);
		}
		else
		{
			Accounts.login();
		}
	}

	public static void drop(Long id)
	{
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));

		User friend = User.findById(id);

		user.unfriend(friend);
		Logger.info("Dropping " + friend.email);
		index();
	}

	public static void byDate(User user)
	{
		String userId = session.get("logged_in_userid");
		user = User.findById(Long.parseLong(userId));

		ArrayList<ArrayList<Message>> conversations = new ArrayList<>();
		ArrayList<Message> sortedList = new ArrayList<Message>();
		sortedList.addAll(user.inbox);
		Collections.sort(sortedList, new MessageDateComparator());
		conversations.add(sortedList);
		render("home/index.html", user, conversations);
	}

	public static void byUser(User user)
	{
		String userId = session.get("logged_in_userid");
		user = User.findById(Long.parseLong(userId));

		ArrayList<ArrayList<Message>> conversations = new ArrayList<>();
		ArrayList<Message> sortedList = new ArrayList<Message>();
		sortedList.addAll(user.inbox);
		Collections.sort(sortedList, new MessageFromComparator());
		conversations.add(sortedList);
		render("home/index.html", user, conversations);

	}

	public static void byConversation(User user)
	{  

		String userId = session.get("logged_in_userid");
		user = User.findById(Long.parseLong(userId));

		ArrayList<ArrayList<Message>> conversations = new ArrayList<>();

		for (Friendship f: user.friendships)
		{
			conversations.add(getConversation(user, f.targetUser)); 
		}

		render("home/index.html", user, conversations);


	}

	static ArrayList<Message> getConversation (User user, User friend)
	{


		ArrayList<Message> conversation = new ArrayList<>();

		for(Message m: user.outbox)
		{
			if(m.to == friend)
			{
				conversation.add(m);
			}
		}

		for(Message m: user.inbox)
		{
			if(m.from == friend)
			{
				conversation.add(m);
			}
		}

		Collections.sort(conversation, new MessageDateComparator());
		return conversation;
	}
}

